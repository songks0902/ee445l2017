
#include "..//inc//tm4c123gh6pm.h"
#include "Switch.h"
#include <stdint.h>
#include "timer.h"





void Init_Switch(){
	volatile unsigned long delay;
	
	SYSCTL_RCGCGPIO_R |= 0x02; 
	while((SYSCTL_PRGPIO_R&0x02) == 0) {
	} 
	
	GPIO_PORTB_DEN_R |= 0x07;
	GPIO_PORTB_DIR_R &= ~0x07;
	/*disable analog*/
	GPIO_PORTB_AMSEL_R &= ~0x07;
	GPIO_PORTB_AFSEL_R &= ~0x07;
	/*set to pull down interface*/
	GPIO_PORTB_PDR_R |= 0x07;
	
	/*PB0-2 are edge sensitive*/
	GPIO_PORTB_IS_R &= ~0x07;
	GPIO_PORTB_IBE_R &= ~0x07;
	GPIO_PORTB_IEV_R &= ~0x07;
	
	/*clear the interrupt*/
	GPIO_PORTB_ICR_R = 0x07;
	
	/*enable*/
	GPIO_PORTB_IM_R |= 0x07;
	
	
	NVIC_PRI0_R = (NVIC_PRI0_R&0xFFFF00FF)|0x00004000; // bits 29-31
	NVIC_EN0_R |= 0x00000002;// enable interrupt 3 in NVIC
	
}


void GPIOPortB_Handler(void) {
	/*PB0*/
	if(GPIO_PORTB_RIS_R&0x01) {
		GPIO_PORTB_ICR_R = 0x01;
		Play_Pause_music();
		
	}
	
	/*PB1*/
	if(GPIO_PORTB_RIS_R&0x02) {
		GPIO_PORTB_ICR_R = 0x02;
		Rewind_music();
		
	}
	
	/*PB2*/
	if(GPIO_PORTB_RIS_R&0x04) {
		GPIO_PORTB_ICR_R = 0x04;
		/*implement later*/
		change_tempo();
		
	}
	
	
}


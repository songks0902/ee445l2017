
#include <stdint.h>


//#define C_2 65.406 			
//#define DF_1 69.296 			
//#define D_1 73.416 			
//#define EF_1  77.782 			
//#define E_1 82.407 			
//#define F_1 87.307 			
//#define GF_1 92.499 			
//#define G_1 97.999 			
//#define AF_1 103.826 			
//#define A_1 110.000 			
//#define BF_1 116.541 			
//#define B_1 123.471 			
//#define C_1 130.813 			
//#define DF0  138.591 			
//#define D0 146.832 			
//#define EF0 155.563 			
//#define E0 164.814 			
//#define F0 174.614 			
//#define GF0 184.997 			
//#define G0 195.998 			
//#define AF0 207.652 			
//#define A0 220.000 			
//#define BF0 233.082 			
//#define B0 246.942 			
//#define C0 261.626 			
//#define DF 277.183 			
//#define D 293.665 			
//#define EF 311.127 			
//#define E 329.628 			
//#define F 349.228 			
//#define GF 369.994 			
//#define G 391.995 			
//#define AF 415.305 			
//#define A 440.000 			
//#define BF 466.164 			
//#define B 493.883 			
//#define C 523.251 			
//#define DF1 554.365 			
//#define D1 587.330 			
//#define EF1 622.254 			
//#define E1 659.255 			
//#define F1 698.456 			
//#define GF1 739.989 			
//#define G1 783.991 			
//#define AF1 830.609 			
//#define A1 880.000 			
//#define BF1 932.328 			
//#define B1 987.767 			
//#define C1 1046.502 			
//#define DF2 1108.731 			
//#define D2 1174.659 			
//#define EF2 1244.508 			
//#define E2 1318.510 			
//#define F2 1396.913 			
//#define GF2 1479.978 			
//#define G2 1567.982 			
//#define AF2 1661.219 			
//#define A2 1760.000 			
//#define BF2 1864.655 			
//#define B2 1975.533 			
//#define C2 2093.005 			

#define C_2 11945   // 65.406 Hz				
#define DF_1 11274   // 69.296 Hz				
#define D_1 10641   // 73.416 Hz				
#define EF_1 10044   // 77.782 Hz				
#define E_1 9480   // 82.407 Hz				
#define F_1 8948   // 87.307 Hz				
#define GF_1 8446   // 92.499 Hz				
#define G_1 7972   // 97.999 Hz				
#define AF_1 7525   // 103.826 Hz				
#define A_1 7102   // 110.000 Hz				
#define BF_1 6704   // 116.541 Hz				
#define B_1 6327   // 123.471 Hz				
#define C_1 5972   // 130.813 Hz				
#define DF0 5637   // 138.591 Hz				
#define D0 5321   // 146.832 Hz				
#define EF0 5022   // 155.563 Hz				
#define E0 4740   // 164.814 Hz				
#define F0 4474   // 174.614 Hz				
#define GF0 4223   // 184.997 Hz				
#define G0 3986   // 195.998 Hz				
#define AF0 3762   // 207.652 Hz				
#define A0 3551   // 220.000 Hz				
#define BF0 3352   // 233.082 Hz				
#define B0 3164   // 246.942 Hz				
#define C0 2986   // 261.626 Hz				
#define DF 2819   // 277.183 Hz				
#define D 2660   // 293.665 Hz				
#define EF 2511   // 311.127 Hz				
#define E 2370   // 329.628 Hz				
#define F 2237   // 349.228 Hz				
#define GF 2112   // 369.994 Hz				
#define G 1993   // 391.995 Hz				
#define AF 1881   // 415.305 Hz				
#define A 1776   // 440.000 Hz				
#define BF 1676   // 466.164 Hz				
#define B 1582   // 493.883 Hz				
#define C 1493   // 523.251 Hz				
#define DF1 1409   // 554.365 Hz				
#define D1 1330   // 587.330 Hz				
#define EF1 1256   // 622.254 Hz				
#define E1 1185   // 659.255 Hz				
#define F1 1119   // 698.456 Hz				
#define GF1 1056   // 739.989 Hz				
#define G1 997   // 783.991 Hz				
#define AF1 941   // 830.609 Hz				
#define A1 888   // 880.000 Hz				
#define BF1 838   // 932.328 Hz				
#define B1 791   // 987.767 Hz				
#define C1 747   // 1046.502 Hz				
#define DF2 705   // 1108.731 Hz				
#define D2 665   // 1174.659 Hz				
#define EF2 628   // 1244.508 Hz				
#define E2 593   // 1318.510 Hz				
#define F2 559   // 1396.913 Hz				
#define GF2 528   // 1479.978 Hz				
#define G2 498   // 1567.982 Hz				
#define AF2 470   // 1661.219 Hz				
#define A2 444   // 1760.000 Hz				
#define BF2 419   // 1864.655 Hz				
#define B2 395   // 1975.533 Hz				
#define C2 373   // 2093.005 Hz				



#define Rest 0


/*beat*/
#define whole 16
#define three_quarter 12
#define half  8 
#define quarter 4
#define eighth 2
#define sixteenth 1

#define Tempo 120

typedef struct Note {
	
	uint16_t frequency;
	uint8_t beat;
	
} Note;






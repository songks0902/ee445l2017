/*https://music.stackexchange.com/questions/42694/what-is-a-chord-in-terms-of-frequencies*/

#include "music.h"




Note Four_Season_Spring_1[240] = {
	{Rest, quarter},
	
	/*section 1*/
	/*1.1*/
	{C, quarter}, 
	{E, quarter}, {E, quarter}, {E, quarter}, {D, eighth}, {C, eighth}, 
	{G, three_quarter}, {G, eighth}, {F, eighth}, 
	{E, quarter}, {E, quarter}, {E, quarter}, {D, eighth}, {C, eighth},
	{G, three_quarter}, {G, eighth}, {F, eighth},
	
	/*1.2*/
	{C, whole},
	{C, quarter}, {C, quarter}, {C, quarter}, {G, eighth},
	{C, three_quarter}, {E, eighth}, {D, eighth},
	{C, quarter}, {C, quarter}, {C, quarter}, {G, eighth},
	{C, three_quarter}, {E, eighth}, {D, eighth},
	
	/*1.3*/
	{C, whole},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	
	/*section 2*/
	/*2.1*/
	{E, quarter}, {F, eighth}, {G, eighth}, {F, quarter}, {E, quarter},
	{D, half}, {Rest, quarter}, {C, quarter},
	{E, quarter}, {E, quarter}, {E, quarter}, {D, eighth}, {C, eighth},
	{G, three_quarter}, {G, eighth}, {F, eighth},
	{E, quarter}, {E, quarter}, {E, quarter}, {D, eighth}, {C, eighth},
	
	/*2.2*/
	{C, quarter}, {D, eighth}, {E, eighth}, {D, quarter}, {C, quarter},
	{D, quarter}, {B, quarter}, {G, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {G, quarter},
	{C, three_quarter}, {E, eighth}, {D, eighth},
	{C, quarter}, {C, quarter}, {C, quarter}, {G, quarter},
	
	/*2.3*/
	{C, quarter}, {C, quarter}, {F, quarter}, {F, quarter},
	{G, three_quarter}, {Rest, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
		
	/*section 3*/
	/*3.1*/
	{G, three_quarter}, {G, eighth}, {F, eighth},
	{E, quarter}, {F, eighth}, {G, eighth}, {F, quarter}, {E, quarter},
	{D, half}, {Rest, quarter}, {E, quarter},
	{G, quarter}, {F, eighth}, {E, eighth}, {F, quarter}, {G, quarter},
	
	/*3.2*/
	{C, three_quarter}, {E, eighth}, {D, eighth},
	{C, quarter}, {D, eighth}, {E, eighth}, {A, quarter}, {A, quarter},
	{B, quarter}, {B, quarter}, {G, quarter}, {E, quarter},
	{G, quarter}, {D, eighth}, {C, eighth}, {F, quarter}, {G, quarter},
	
	/*3.3*/
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {F, quarter}, {F, quarter},
	{G, three_quarter}, {Rest, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	
	/*section 4*/
	/*4.1*/
	{A, quarter}, {G, half}, {C, quarter},
	{G, quarter}, {F, eighth}, {E, eighth}, {F, quarter}, {G, quarter},
	{A, quarter}, {G, half}, {C, quarter},
	{A, quarter}, {G, half}, {F, quarter},
	{E, quarter}, {D, eighth}, {C, eighth}, {D, quarter}, {C, quarter},
	
	/*4.2*/
	{A, quarter}, {G, half}, {E, quarter},
	{G, quarter}, {D, eighth}, {C, eighth}, {F, quarter}, {G, quarter},
	{F, quarter}, {E, half}, {C, quarter},
	{A, quarter}, {G, half}, {A, quarter},
	{E, quarter}, {C, quarter}, {D, half},
	
	/*4.3*/
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {G, quarter}, {G, quarter},
	
	
	{Rest, quarter}
};


Note Four_Season_Spring_2[240] = {
	{Rest, 0},
	
	/*section 1*/
	/*1.1*/
	{Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0},
	
	/*1.2*/
	{Rest, 0},
	{E, quarter}, {E, quarter}, {E, quarter}, {Rest, 0},
	{E, three_quarter}, {Rest, 0}, {Rest, 0},
	{E, quarter}, {E, quarter}, {E, quarter}, {Rest, 0},
	{E, three_quarter}, {Rest, 0}, {Rest, 0},
	
	/*1.3*/
	{Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	
	/*section 2*/
	/*2.1*/
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	
	/*2.2*/
	{E, quarter}, {Rest, 0}, {Rest, 0}, {A, quarter}, {A, quarter},
	{B, quarter}, {Rest, 0}, {Rest, 0}, {E, quarter},
	{E, quarter}, {E, quarter}, {E, quarter}, {Rest, 0},
	{E, three_quarter}, {Rest, 0}, {Rest, 0},
	{E, quarter}, {E, quarter}, {E, quarter}, {Rest, 0},
	
	/*2.3*/
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	
	
	/*section 3*/
	/*3.1*/
	{Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	
	/*3.2*/
	{E, three_quarter}, {Rest, 0}, {Rest, 0},
	{E, quarter}, {Rest, 0}, {Rest, 0}, {D, quarter}, {C, quarter},
	{D, quarter}, {Rest, 0}, {Rest, 0}, {C, quarter},
	{E, quarter}, {Rest, 0}, {Rest, 0}, {D, quarter}, {E, quarter},
	
	/*3.3*/
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	
	
	/*section 4*/
	/*4.1*/
	{Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	{Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0},
	{Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, {Rest, 0}, 
	
	/*4.2*/
	{F, quarter}, {E, half}, {C, quarter},
	{E, quarter}, {Rest, 0}, {Rest, 0}, {D, quarter}, {E, quarter},
	{A, quarter}, {G, half}, {Rest, 0},
	{F, quarter}, {E, half}, {F, quarter},
	{G, quarter}, {Rest, 0}, {A, half},
	
	/*4.3*/
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {C, quarter}, {C, quarter},
	{C, quarter}, {C, quarter}, {Rest, 0}, {Rest, 0},
	
	{Rest, 0}
};




uint8_t volume_1[240] = {
	0,
	
	/*section 1*/
	/*1.1*/
	1,
	1,1,1,1,1,
	1,1,1,
	1,1,1,1,1,
	1,1,1,

	/*1.2*/
	1,
	1,1,1,1,
	1,1,1,
	1,1,1,1,
	1,1,1,
	
	/*1.3*/
	1,
	1,1,1,1,
	1,1,1,1,
	1,1,1,1,
	1,1,1,1,
	
	/*section 2*/
	/*2.1*/
	1,1,1,1,1,
	1, 0, 1,
	1,1,1,1,1,
	1,1,1,
	1,1,1,1,1,

	/*2.2*/
	1,1,1,1,1,
	1,1,1,1,
	1,1,1,1,
	1,1,1,
	1,1,1,1,
	
	/*2.3*/
	1,1,1,1,
	1,0,
	1,1,1,1,
	1,1,1,1,
	1,1,1,1,
	
	/*section 3*/
	/*3.1*/
	1,1,1,
	1,1,1,1,1,
	1,1,1,
	1,1,1,1,1,

	/*3.2*/
	1,1,1,
	1,1,1,1,1,
	1,1,1,1,
	1,1,1,1,1,
	
	/*3.3*/
	1,1,1,1,
	1,1,1,1,
	1,0,
	1,1,1,1,
	
	/*section 4*/
	/*4.1*/
	1,1,1,
	1,1,1,1,1,
	1,1,1,
	1,1,1,
	1,1,1,1,1,
	
	/*4.2*/
	1,1,1,
	1,1,1,1,1,
	1,1,1,
	1,1,1,
	1,1,1,
	
	/*4.3*/
	1,1,1,1,
	1,1,1,1,
	1,1,1,1,
	1,1,1,1,
	1,1,1,1,
	
	/*end*/
	0,
	
};

void Music_Init() {
	
	Spring.musicName = "The Four Season: Spring";
	Spring.notes = Four_Season_Spring_1;
	Spring.tempo = 90; /*recommended tempo is 90-100*/
	Spring.volumes = volume_1;
	Spring.instru = Wave;
	Spring.len = 240;
	
	Spring_2.musicName = "The Four Season: Spring";
	Spring_2.notes = Four_Season_Spring_2;
	Spring_2.tempo = 90;
	Spring_2.volumes = volume_1;
	Spring_2.instru = Wave;
	Spring_2.len = 240;
	
	
}


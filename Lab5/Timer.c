#include "..//inc//tm4c123gh6pm.h"
#include "timer.h"
#include "DAC.h"


void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void); // low power mode



uint32_t envelope = 1;

uint32_t note_index = 0;
uint32_t instr_count_1 = 0;
uint32_t output = 0;

uint32_t instr_count_2 = 0;
//uint32_t output = 0;

uint32_t t1_out = 0;
uint32_t t2_out = 0;

uint32_t time = 0;

uint32_t buff[32];
uint32_t count = 0;
/*bool*/
uint8_t wait = 1;
uint8_t status  = 1;
/*Timer Init*/

void Timer_Init_1(uint32_t period) {

	SYSCTL_RCGCTIMER_R |= 0x02;   // 0) activate TIMER1
  TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup
  TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER1_TAILR_R = period-1;    // 4) reload value
  TIMER1_TAPR_R = 0;            // 5) bus clock resolution
  TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER1A timeout flag
  TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|0x00008000; // 8) priority 4
// interrupts enabled in the main program after all devices initialized
// vector number 37, interrupt number 21
  NVIC_EN0_R = 1<<21;           // 9) enable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000001; // 10) enable TIMER1A
	
}

void Timer_Init_2(uint32_t period) {
	
	SYSCTL_RCGCTIMER_R |= 0x04;   // 0) activate timer2
 
  TIMER2_CTL_R = 0x00000000;    // 1) disable timer2A during setup
  TIMER2_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER2_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER2_TAILR_R = period-1;    // 4) reload value
  TIMER2_TAPR_R = 0;            // 5) bus clock resolution
  TIMER2_ICR_R = 0x00000001;    // 6) clear timer2A timeout flag
  TIMER2_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0x00FFFFFF)|0x80000000; // 8) priority 4
// interrupts enabled in the main program after all devices initialized
// vector number 39, interrupt number 23
  NVIC_EN0_R = 1<<23;           // 9) enable IRQ 23 in NVIC
	TIMER2_CTL_R = 0x00000001; // 10) enable timer2A
	
}

void Timer_Init_3(uint32_t period) {
	
	SYSCTL_RCGCTIMER_R |= 0x08;   // 0) activate TIMER3      
  TIMER3_CTL_R = 0x00000000;    // 1) disable TIMER3A during setup
  TIMER3_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER3_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER3_TAILR_R = period-1;    // 4) reload value
  TIMER3_TAPR_R = 0;            // 5) bus clock resolution
  TIMER3_ICR_R = 0x00000001;    // 6) clear TIMER3A timeout flag
  TIMER3_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI8_R = (NVIC_PRI8_R&0x00FFFFFF)|0x80000000; // 8) priority 4
// interrupts enabled in the main program after all devices initialized
// vector number 51, interrupt number 35
  NVIC_EN1_R = 1<<(35-32);      // 9) enable IRQ 35 in NVIC
	TIMER3_CTL_R = 0x00000001; // 10) enable TIMER3A
	
	
	
}

void Systick_Init(uint32_t period) {
	//period = 1402;
	long sr;
  sr = StartCritical();
  NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
  NVIC_ST_RELOAD_R = period-1;// reload value
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
  NVIC_SYS_PRI3_R = (NVIC_SYS_PRI3_R&0x00FFFFFF)|0x40000000; // priority 2
                              // enable SysTick with core clock and interrupts
  NVIC_ST_CTRL_R = 0x07;
	EndCritical(sr);
	
}



/************************************/


void Init_Timer() {
	
	
	Systick_Init(Spring.notes[note_index].frequency/2);
	/*init timer1 for play track 1*/
	Timer_Init_1(Spring.notes[note_index].frequency);
	/*init timer1 for play track 2*/
	Timer_Init_2(Spring_2.notes[note_index].frequency);
	/*init timer3 for duration*/
	uint16_t tempo = Spring.tempo;
	//double sixteen_len = (60*50000000)/(4*tempo);
	
	time = tempo*52083;
	//time = 6250000;
	Timer_Init_3(Spring.notes[note_index].beat*time);
	
//	TIMER1_CTL_R = 0x01;
//	TIMER2_CTL_R = 0x01;
//	TIMER3_CTL_R = 0x01;
//	NVIC_ST_CTRL_R = 0x07;
//	NVIC_ST_CTRL_R = 0;
	
}

/*Timer Handler*/
void Timer1A_Handler(void){
  TIMER1_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER1A timeout
	uint16_t out = Spring.instru[instr_count_1];
	if(envelope) {
		uint32_t check = Spring.notes[note_index].beat * time - TIMER3_TAR_R;
		uint32_t c = Spring.notes[note_index].beat * time;
		if(check < c/8) {
			out = (out * (check) / (c/8));		// start envelope
		} else {
			out = (out *  (c) / check);			// end envelope
		}
	}
	instr_count_1 = (instr_count_1+1) % 32;
	t1_out = out;
	
	
}

void Timer2A_Handler(void){
  TIMER2_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER2A timeout
	
	uint16_t out = Spring_2.instru[instr_count_2];
	if(envelope) {
	uint32_t check = Spring_2.notes[note_index].beat * time - TIMER3_TAR_R;
	uint32_t c = Spring_2.notes[note_index].beat * time;
		if(check < c/8) {
			out = (out * (check) / (c/8));		// start envelope
		} else {
			out = (out *  (c/8) / check);			// end envelope
		}
}
	instr_count_2 = (instr_count_2+1) % 32;
	t2_out = out;
	
	
}


/*go to the next node*/
void Timer3A_Handler(void){
  /*set a duration*/
	if(envelope == 0){
	long sr;
  sr = StartCritical();
	int count = 10000;
	while(count >= 0) {
		count -= 1;
	}	
	EndCritical(sr);
}
	TIMER3_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER3A timeout
	note_index += 1;
	
	if(Spring.notes[note_index].frequency == 0) { //if the note is a rest note
		
		TIMER1_CTL_R = 0x00;
		//Timer1_Period(Spring.notes[note_index_1].frequency);
		t1_out = 0;
		
		
	}else {
		//TIMER1_CTL_R = 0x00;
		//Timer1_Period(Spring.notes[note_index_1].frequency);
		TIMER1_CTL_R = 0x01;
		instr_count_1 = 0;
		
	}
	Timer1_Period(Spring.notes[note_index].frequency);
	
	if(Spring_2.notes[note_index].frequency == 0) {
		
		TIMER2_CTL_R = 0x00;
		//Timer2_Period(Spring_2.notes[note_index_2].frequency);
		t2_out = 0;
		
	}else {
		//TIMER2_CTL_R = 0x00;
		//Timer2_Period(Spring_2.notes[note_index_2].frequency);
		TIMER2_CTL_R = 0x01;
		instr_count_2 = 0;
	
	}
	
	Timer2_Period(Spring_2.notes[note_index].frequency);
	
	Timer3_Period((uint32_t)Spring.notes[note_index].beat*time);
	Systick_Period(Spring.notes[note_index].frequency/2);
	if((note_index - 1) >= Spring.len) {
		Rewind_music();
		
	}
	
	
}

/*systick handler*/
void SysTick_Handler() {
	
	uint32_t out = 0;
	
	if(t1_out !=0 && t2_out!= 0){
		
		out = (t1_out/2) + (t2_out/2);
	} else if(t1_out != 0) {
		
		out = t1_out;
	} else if (t2_out != 0) {
		
		out = t2_out;
	}
	
	DAC_Out(out);
}



/****************************************************/
/*stop the music player and restart*/
void Rewind_music() {
	
	status = 1;
	note_index = 0;
	
	TIMER1_CTL_R = 0x00;
	TIMER2_CTL_R = 0x00;
	TIMER3_CTL_R = 0x00;
	NVIC_ST_CTRL_R &= ~0x07;
	
}	

void Play_Pause_music() {
	
	/*pause*/
	if(status == 0) {
		TIMER1_CTL_R = 0x00;
		TIMER2_CTL_R = 0x00;
		TIMER3_CTL_R = 0x00;
		NVIC_ST_CTRL_R = 0;
		status = 1;
	}
	/*play*/
	else if(status == 1) {
		TIMER1_CTL_R = 0x01;
		TIMER2_CTL_R = 0x01;
		TIMER3_CTL_R = 0x01;
		NVIC_ST_CTRL_R = 0x07;
		
		status = 0;
		
	}
}


void change_tempo() {
	if(envelope){
		envelope = 0;
		time = 150*52083;
	}else {
		
		envelope = 1;
		time = 120*52083;
	}
	
}


/****************************************************/


/*change the period*/
void Timer1_Period(uint32_t period) {
	/*if(period == 0) {
		period = 1;
	}*/
	TIMER1_TAILR_R = period - 1;
	
}

void Timer2_Period(uint32_t period) {
	/*if(period == 0) {
		period = 1;
	}*/
	TIMER2_TAILR_R = period - 1;
	
}


void Timer3_Period(uint32_t period) {
	TIMER3_TAILR_R = period - 1;
	output = TIMER3_TAILR_R;
}

void Systick_Period(uint32_t period) {
	if(period == 0) {
		period = 1;
	}
	//period = 1402;
	NVIC_ST_CTRL_R = 0x00;
	NVIC_ST_RELOAD_R = period-1;
	if(period >= 1) {
		NVIC_ST_CTRL_R = 0x07;
	}
}




#include <stdint.h>
#include "music.h"


extern music Spring;

void Init_Timer(void);

void Timer_Init_1(uint32_t period);

void Timer_Init_2(uint32_t period);

void Timer_Init_3(uint32_t period);

void Timer1_Period(uint32_t period);
void Timer2_Period(uint32_t period);
void Timer3_Period(uint32_t period);



void Systick_Init(uint32_t period);

void SysTick_Handler(void);

void Rewind_music(void);

void Play_Pause_music(void);

void change_tempo(void);

void Systick_Period(uint32_t period);

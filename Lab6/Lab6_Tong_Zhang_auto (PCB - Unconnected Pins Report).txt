Unconnected Pins Report
-----------------------

Report File:        C:\Users\tony zhang\Desktop\EE445L\ee445l2017\Lab6\Lab6_Tong_Zhang_auto (PCB - Unconnected Pins Report).txt
Report Written:     Tuesday, October 17, 2017
Design Path:        C:\Users\tony zhang\Desktop\EE445L\ee445l2017\Lab6\Lab6_Tong_Zhang_auto.pcb
Design Title:       
Created:            1/29/2013 12:01:41 AM
Last Saved:         10/17/2017 2:13:27 PM
Editing Time:       1152 min
Units:              mil (precision 0)

Component J3 Pin J3-9 (Chan7)
Component J3 Pin J3-10 (Chan6)
Component J3 Pin J3-11 (Chan5)
Component J3 Pin J3-12 (Chan4)
Component J3 Pin J3-13 (Chan3)
Component J3 Pin J3-14 (Chan2)
Component J3 Pin J3-15 (Chan1)
Component J3 Pin J3-16 (Chan0)
Component U1 Pin U1-1 (PB6)
Component U1 Pin U1-5 (PF4)
Component U1 Pin U1-7 (PE2)
Component U1 Pin U1-8 (PE1)
Component U1 Pin U1-9 (PE0)
Component U1 Pin U1-10 (PD7)
Component U1 Pin U1-13 (PC7)
Component U1 Pin U1-14 (PC6)
Component U1 Pin U1-15 (PC5)
Component U1 Pin U1-16 (PC4)
Component U1 Pin U1-18 (PA1)
Component U1 Pin U1-28 (PF0)
Component U1 Pin U1-29 (PF1)
Component U1 Pin U1-30 (PF2)
Component U1 Pin U1-31 (PF3)
Component U1 Pin U1-33 (HIB*)
Component U1 Pin U1-36 (XOSC1)
Component U1 Pin U1-43 (PD4)
Component U1 Pin U1-44 (PD5)
Component U1 Pin U1-45 (PB0)
Component U1 Pin U1-46 (PB1)
Component U1 Pin U1-47 (PB2)
Component U1 Pin U1-48 (PB3)
Component U1 Pin U1-53 (PD6)
Component U1 Pin U1-61 (PD0)
Component U1 Pin U1-62 (PD1)
Component U1 Pin U1-63 (PD2)
Component U1 Pin U1-64 (PD3)
Component J5 Pin J5-4 (Id)
Component J5 Pin J5-3 (D+)
Component J5 Pin J5-2 (D-)
Component J5 Pin J5-6 (m1)
Component J5 Pin J5-7 (m2)
Component J5 Pin J5-8 (m3)
Component J5 Pin J5-9 (m4)

Unconnected pins found : 43

End Of Report.

// main.c
// Runs on LM4F120/TM4C123
// UART runs at 115,200 baud rate 
// Daniel Valvano
// May 3, 2015

/* This example accompanies the books
  "Embedded Systems: Introduction to ARM Cortex M Microcontrollers",
  ISBN: 978-1469998749, Jonathan Valvano, copyright (c) 2015

"Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015
 
 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */


#include <stdint.h> // C99 variable types
#include "ADCSWTrigger.h"
#include "uart.h"
#include "PLL.h"
#include "../inc/tm4c123gh6pm.h"
#include "ST7735.h"
#include "convert.h"
//#include <stdio.h>
int32_t ADC_FIFO_Pop(void);
int32_t* ADC_FIFO_Get(void);
int32_t ADC_FIFO_CurrentValue(void);
void ADC_FIFO_Push(int32_t data);

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode

volatile uint32_t ADCvalue;
uint32_t AdcArray[1000];
uint32_t recorded = 0;


#define SIZE 160

/*build a fifo for the values*/
int32_t fifo[160];
int32_t start = 0;
int32_t end = 0;


int32_t ADC_FIFO_Pop() {
	if(start == end) {
		return -1;
	}
	int32_t value = fifo[start];
	start = (start+1) % SIZE;
	
	return value;
	
}

void ADC_FIFO_Push(int32_t data) {
	
	int32_t next = (end+1) %SIZE;
	if(next == start) {
		return;
	}		
	
	end = next;
	
	fifo[end] = data;
}

int32_t* ADC_FIFO_Get(){
	return fifo;
	
}

int32_t ADC_FIFO_CurrentValue() {
	return fifo[start];
	
}



void Timer0A_Handler(void)
{
    TIMER0_ICR_R = TIMER_ICR_TATOCINT; // acknowledge timer0A timeout
		int32_t temp = (start + 1) % SIZE;
		if(temp == end) {
			ADC_FIFO_Pop();
		}
	
    ADCvalue = ADC0_InSeq3();
		ADC_FIFO_Push(ADCvalue);
  
}

void Timer0A_Init1000HzInt(void)
{
    volatile uint32_t delay;
    // **** general initialization ****
    SYSCTL_RCGCTIMER_R |= 0x01;      // activate timer0
    delay = SYSCTL_RCGCTIMER_R;      // allow time to finish activating
    TIMER0_CTL_R &= ~TIMER_CTL_TAEN; // disable timer0A during setup
    TIMER0_CFG_R = 0;                // configure for 32-bit timer mode
    // **** timer0A initialization ****
    // configure for periodic mode
    TIMER0_TAMR_R = TIMER_TAMR_TAMR_PERIOD;
    TIMER0_TAILR_R = 79999;            // start value for 1000 Hz interrupts
    TIMER0_IMR_R |= TIMER_IMR_TATOIM;  // enable timeout (rollover) interrupt
    TIMER0_ICR_R = TIMER_ICR_TATOCINT; // clear timer0A timeout flag
    TIMER0_CTL_R |= TIMER_CTL_TAEN;    // enable timer0A 32-b, periodic, interrupts
    // **** interrupt initialization ****
    // Timer0A=priority 2
    NVIC_PRI4_R = (NVIC_PRI4_R & 0x00FFFFFF) | 0x40000000; // top 3 bits
    NVIC_EN0_R = 1 << 19;                                  // enable interrupt 19 in NVIC
}


int main(void){
	
	DisableInterrupts();
  PLL_Init(Bus80MHz);
	ADC0_InitSWTriggerSeq3_Ch9();
	Timer0A_Init1000HzInt();
	UART_Init();
	ST7735_InitR(INITR_REDTAB);
	EnableInterrupts();
  while(1){
//    data = ADC0_InSeq3();
//    UART_OutString("\n\rADC data =");
//    UART_OutUDec(data);
		ST7735_SetCursor(0, 0);
		int32_t Temperature = Convert_Adcvalue(ADC_FIFO_CurrentValue());
		ST7735_sDecOut3(Temperature);
		//printf("Temperature %5d.%02d\n", Temperature / 100, Temperature % 100);
		ST7735_PlotClear(32, 159);
		
		int i = 0;
		for(i = 0; i < SIZE; i+=1) {
			int32_t point = 128 - Convert_Adcvalue(ADC_FIFO_Get()[i]) * 2 / 100;
			ST7735_DrawPixel(i+1, point+1, ST7735_RED);
			ST7735_DrawPixel(i+1, point, ST7735_RED);
			ST7735_DrawPixel(i, point+1, ST7735_RED);
			ST7735_DrawPixel(i, point, ST7735_RED);
			
			
		}
		
		WaitForInterrupt();
		
		
  }
}



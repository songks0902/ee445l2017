// ******** fixed.c ************** 
// possible header file for Lab 1 
// feel free to change the specific syntax of your system
// Kaisheng Song, Tong Zhang
// Description: Using ARM Cortex M processor to develop fixed-point output routines
// Lab Number: TTu 2-330
// TA: 
// Initial Creation Date: 09/05/2017
// Date of last revision: 09/05/2016

#include <stdint.h>
#include "fixed.h"
#include "ST7735.h"

int maxX, maxY, minX, minY, xSlope, xRange, ySlope, yRange;

/****************ST7735_sDecOut3***************
 converts fixed point number to LCD
 format signed 32-bit with resolution 0.001
 range -9.999 to +9.999
 Inputs:  signed 32-bit integer part of fixed-point number
 Outputs: none
 send exactly 6 characters to the LCD 
Parameter LCD display
 12345    " *.***"
  2345    " 2.345"  
 -8100    "-8.100"
  -102    "-0.102" 
    31    " 0.031" 
-12345    " *.***"
 */
void ST7735_sDecOut3(int32_t n){
		//max = 9999 min = -9999
		if(n < -9999 || n > 9999) {
			//output *.***
			ST7735_OutChar(32);
			ST7735_OutChar(42);
			ST7735_OutChar(46);
			ST7735_OutChar(42);
			ST7735_OutChar(42);
			ST7735_OutChar(42);
			
			return;
		}
		
		//if number is negative output "-" instead of " "
		if(n < 0){
			ST7735_OutChar(45);
			n *= -1; //so we can handle the number as a positive 
		} else {
			ST7735_OutChar(32);
		}
		
		int divisor = 1000;
		int output;
		
		while(divisor > 0){
			output = n/divisor;
			ST7735_OutChar(output+48);
			//check if decimal point needed
			if(divisor == 1000) {
				ST7735_OutChar(46);
			}
			n %= divisor;
			divisor /= 10;
			
			
		}
}

/*
 * @brief 	switch the inout number to unsigned 32-bit binary fixed-point ?= 1/256
 * @param int32_t n		input number
 * @ouput print out the number on the LCD screen
 */
void ST7735_uBinOut8(uint32_t n){
		if(n>=256000)
		//output *.***
		{
			ST7735_OutChar(42);
			ST7735_OutChar(42);
			ST7735_OutChar(42);
			ST7735_OutChar(46);
			ST7735_OutChar(42);
			ST7735_OutChar(42);
			return;
		}
		
		int number = n * 125 / 32;
		int power = 100000;
		int digit = 0;
		int flag = 0;
		
		while(power > 1)
		{
			digit = number / power;
			number = number % power;
			
			if(digit != 0 || flag)
			{
				flag=1;		//weve started
				ST7735_OutChar(48 + digit);		//display digit
			}
			else
			{
				if(power == 1000)
				{
					ST7735_OutChar(48);
				}
				else
				{
					ST7735_OutChar(32);
				}
			}
			if(power==1000){  //time for period
				flag=2;  //weve stated displaying
				ST7735_OutChar(46);		//period
			}
			
			power /= 10;
		}
}

/*
 * plot is 128 by 128  x plot ranges 0 -> 127 (in pixels)
 * y is 160 large			y plot ranges 32 -> 159 (in pixels)
 */
void ST7735_XYplotInit(char* title, int32_t minx, int32_t maxx, int32_t miny, int32_t maxy){
		ST7735_FillScreen(0);		//clear the screen for this plot
		
		maxX = maxx;
		minX = minx;
		maxY = maxy;
		minY = miny;

		xRange = maxX-minX;	
		xSlope = xRange/127;
		if(xRange-xSlope*127>32){
			xSlope += 1;
		}
		
		yRange = maxY-minY;
		ySlope = yRange/127;
		if(yRange-ySlope*127>32){
			ySlope++;
		}
		
		ST7735_DrawString(1,1,title,0x07FF);
		
		if(maxX<0){
			ST7735_DrawFastVLine(127,32,128,0x077FF);
		}
		else if(minX<0){
			ST7735_DrawFastVLine((minX*-1)/xSlope, 32, 128, 0x07FF);
		}
		else{
			ST7735_DrawFastVLine(0,32,128,0x07FF);
		}
		
		if(maxY<0){
			ST7735_DrawFastHLine(0,32,128,0x07FF);
		}
		else if(minY<0){
			ST7735_DrawFastHLine(0,159-(minY*-1)/ySlope, 128, 0x07FF);
		}
		else{
			ST7735_DrawFastHLine(0, 159, 128, 0x07FF);
		}
}

void ST7735_XYplot(uint32_t num, int32_t bufX[], int32_t bufY[]){
		for(int i = 0; i<num; i++){
				if(bufX[i]<minX || bufX[i]>maxX || bufY[i]<minY || bufY[i]>maxY){
					continue;
				}
				int x = (bufX[i]-minX)/xSlope;
				int y = (bufY[i]-minY)/ySlope;
				ST7735_DrawPixel(x,159 - y,0xFFFF);
		}
}


// ADCTestMain.c
// Runs on LM4F120/TM4C123
// This program periodically samples ADC channel 0 and stores the
// result to a global variable that can be accessed with the JTAG
// debugger and viewed with the variable watch feature.
// Daniel Valvano
// May 18, 2014

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2014

 Copyright 2014 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

// center of X-ohm potentiometer connected to PE3/AIN0
// bottom of X-ohm potentiometer connected to ground
// top of X-ohm potentiometer connected to +3.3V 
#include <stdint.h>
#include "ADCSWTrigger.h"
#include "inc/tm4c123gh6pm.h"
#include "ST7735.h"
#include "PLL.h"

#define GPIO_PORTF2             (*((volatile uint32_t *)0x40025010))
#define GPIO_PORTF1             (*((volatile uint32_t *)0x40025008))


void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode

uint32_t Timearray[1000];
uint32_t ADCarray[1000];

volatile uint32_t ProcessFlag;
volatile uint32_t ADCvalue;

//************* ST7735_Line********************************************
//  Draws one line on the ST7735 color LCD
//  Inputs: (x1,y1) is the start point
//          (x2,y2) is the end point
// x1,x2 are horizontal positions, columns from the left edge
//               must be less than 128
//               0 is on the left, 126 is near the right
// y1,y2 are vertical positions, rows from the top edge
//               must be less than 160
//               159 is near the wires, 0 is the side opposite the wires
//        color 16-bit color, which can be produced by ST7735_Color565() 
// Output: none
void ST7735_Line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, 
                 uint16_t color);



// This debug function initializes Timer0A to request interrupts
// at a 100 Hz frequency.  It is similar to FreqMeasure.c.
void Timer0A_Init100HzInit(void){
  volatile uint32_t delay;
  DisableInterrupts();
  // **** general initialization ****
  SYSCTL_RCGCTIMER_R |= 0x01;// activate timer0
  delay = SYSCTL_RCGCTIMER_R;          // allow time to finish activating
  TIMER0_CTL_R &= ~TIMER_CTL_TAEN; // disable timer0A during setup
  TIMER0_CFG_R = 0;                // configure for 32-bit timer mode
  // **** timer0A initialization ****
                                   // configure for periodic mode
  TIMER0_TAMR_R = TIMER_TAMR_TAMR_PERIOD;
  TIMER0_TAILR_R = 249999;        // start value for 100 Hz interrupts 25Mhz*.01s = 250,000us  . we subtract one for the extra cycle so 249,999
  TIMER0_IMR_R |= TIMER_IMR_TATOIM;// enable timeout (rollover) interrupt
  TIMER0_ICR_R = TIMER_ICR_TATOCINT;// clear timer0A timeout flag
  TIMER0_CTL_R |= TIMER_CTL_TAEN;  // enable timer0A 16-b, periodic, interrupts
  // **** interrupt initialization ****
                                   // Timer0A=priority 2
  NVIC_PRI4_R = (NVIC_PRI4_R&0x00FFFFFF)|0x40000000; // top 3 bits
  NVIC_EN0_R = NVIC_EN0_INT19;     // enable interrupt 19 in NVIC
}

void Timer2_Init(){
  SYSCTL_RCGCTIMER_R |= 0x04;   // 0) activate timer2
  TIMER2_CTL_R = 0x00000000;    // 1) disable timer2A during setup
  TIMER2_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER2_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER2_TAILR_R = 99*25-1;    // 4) reload value
  TIMER2_TAPR_R = 0;            // 5) bus clock resolution
  TIMER2_ICR_R = 0x00000001;    // 6) clear timer2A timeout flag
  TIMER2_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0x00FFFFFF)|0x20000000; // 8) priority 1
// interrupts enabled in the main program after all devices initialized
// vector number 39, interrupt number 23
  NVIC_EN0_R = 1<<23;           // 9) enable IRQ 23 in NVIC
  TIMER2_CTL_R = 0x00000001;    // 10) enable timer2A
}

void Timer2A_Handler(void){
  TIMER2_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER2A timeout
}

void Timer3_Init(){
  SYSCTL_RCGCTIMER_R |= 0x08;   // 0) activate TIMER3
  TIMER3_CTL_R = 0x00000000;    // 1) disable TIMER3A during setup
  TIMER3_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER3_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER3_TAILR_R = 99*25-1;    // 4) reload value
  TIMER3_TAPR_R = 0;            // 5) bus clock resolution
  TIMER3_ICR_R = 0x00000001;    // 6) clear TIMER3A timeout flag
  TIMER3_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI8_R = (NVIC_PRI8_R&0x00FFFFFF)|0x20000000; // 8) priority 1
// interrupts enabled in the main program after all devices initialized
// vector number 51, interrupt number 35
  NVIC_EN1_R = 1<<(35-32);      // 9) enable IRQ 35 in NVIC
  TIMER3_CTL_R = 0x00000001;    // 10) enable TIMER3A
}

void Timer3A_Handler(void){
  TIMER3_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER3A timeout
}


uint32_t record = 0; //entries index

void Timer0A_Handler(void){
  TIMER0_ICR_R = TIMER_ICR_TATOCINT;    // acknowledge timer0A timeout
  GPIO_PORTF2 ^= 0x04;                   // profile
  GPIO_PORTF2 ^= 0x04;                   // profile
  ADCvalue = ADC0_InSeq3();
	
	if(record < 1000){
		Timearray[record] = TIMER1_TAR_R;
		ADCarray[record] = ADCvalue;
		record++;
	}
	else{
		ProcessFlag = 1;
	}
  GPIO_PORTF2 ^= 0x04;                   // profile
}

void Timer1_Init(){
	volatile uint32_t delay;
  SYSCTL_RCGCTIMER_R |= 0x02;   // 0) activate TIMER1
	delay = SYSCTL_RCGCTIMER_R; 
  TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup
  TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER1_TAILR_R = 0xFFFFFFFF-1;    // 4) reload value
  TIMER1_TAPR_R = 0;            // 5) bus clock resolution
  //TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER1A timeout flag
	//TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
	//NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|0x00008000; // 8) priority 4
	//interrupts enabled in the main program after all devices initialized
	//vector number 37, interrupt number 21
	//NVIC_EN0_R = 1<<21;           // 9) enable IRQ 21 in NVIC
  TIMER1_CTL_R = 0x00000001;    // 10) enable TIMER1A
}

uint32_t differences[999];
uint32_t pdf[4096];

int main(void){
  PLL_Init();
  SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOF; // activate port F
  ADC0_InitSWTriggerSeq3(0);            // allow time to finish activating
//  ADC0_InitAllTriggerSeq3(0);           // allow time to finish activating
  Timer0A_Init100HzInit();                // set up Timer0A for 100 Hz interrupts
//  Timer2_Init();
//  Timer3_Init();
  Timer1_Init();
  
  GPIO_PORTF_DIR_R |= 0x06;             // make PF2 out (built-in LED)
  GPIO_PORTF_AFSEL_R &= ~0x06;          // disable alt funct on PF2
  GPIO_PORTF_DEN_R |= 0x06;             // enable digital I/O on PF2
                                        // configure PF2 as GPIO
  GPIO_PORTF_PCTL_R = (GPIO_PORTF_PCTL_R&0xFFFFF0FF)+0x00000000;
  GPIO_PORTF_AMSEL_R = 0;               // disable analog functionality on PF
  GPIO_PORTF2 = 0;                      // turn off LED
  GPIO_PORTF1 = 0;                      // turn off LED
  EnableInterrupts();
  
  while(!ProcessFlag){
    GPIO_PORTF1 ^= 0x02;
	//GPIO_PORTF1 = (GPIO_PORTF1*12345678)/1234567+0x02; 
//	GPIO_PORTF1 = (GPIO_PORTF1*12345678)/1234567+0x02; 
//	GPIO_PORTF1 = (GPIO_PORTF1*12345678)/1234567+0x02; 	
  }
  //ProcessFlag == 1
  
	GPIO_PORTF1 = 0;
	GPIO_PORTF2 = 0;
	
	uint32_t largestdiff = 0;
	uint32_t smallestdiff = 4294967295; //integer max
	uint32_t smallest = 4294967295;
	uint32_t largest = 0;
	
	
	for(int i = 0; i<999; i++){  
		differences[i]=Timearray[i]-Timearray[i+1];
		if(differences[i]<smallestdiff)
			smallestdiff = differences[i];
		else if(differences[i]>largestdiff)
			largestdiff = differences[i];
			
		if(ADCarray[i+1]<smallest)
			smallest = ADCarray[i+1];
		else if(ADCarray[i+1]>largest)
			largest = ADCarray[i+1];
	}
	uint32_t jitter = largestdiff - smallestdiff;
	
	/*for(int i = 0; i<1000; i++){
		pdf[ADCarray[i]-smallest]++;
	}*/
	int breakpoint = 0;
	return 1;
}

int main2(void){
	PLL_Init();                           // 25 MHz
	ST7735_InitR(INITR_REDTAB);
	ST7735_FillScreen(0x07E0);  // set screen to black
	ST7735_SetCursor(0,0);
	
	ST7735_Line(50,80,50,0,0xFFFF); // 12
	ST7735_Line(50,80,80,20,0xFFFF);// 1
	ST7735_Line(50,80,100,80,0xFFFF);// 3
	ST7735_Line(50,80,80,120,0xFFFF);// 5 
	ST7735_Line(50,80,50,150,0xFFFF); // 6
	ST7735_Line(50,80,30,120,0xFFFF); // 7
	ST7735_Line(50,80,0,80,0xFFFF); // 9
	ST7735_Line(50,80,30,40,0xFFFF); // 11
	return  1;
}

int abs(int a){
	if(a<0)
		return a*-1;
	return a;
}

//************* ST7735_Line********************************************
//  Draws one line on the ST7735 color LCD
//  Inputs: (x1,y1) is the start point
//          (x2,y2) is the end point
// x1,x2 are horizontal positions, columns from the left edge
//               must be less than 128
//               0 is on the left, 126 is near the right
// y1,y2 are vertical positions, rows from the top edge
//               must be less than 160
//               159 is near the wires, 0 is the side opposite the wires
//        color 16-bit color, which can be produced by ST7735_Color565() 
// Output: none
void ST7735_Line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, 
                 uint16_t color){
				 
	if(abs(x2-x1)<=abs(y2-y1)){ //find the larger value, that way we can iterate over that one
		if(y1>y2){ //since our loop increases by 1 pixel each time we need to start with the lower value
			int swap = y1;
			y1 = y2;
			y2 = swap;
			swap = x1; //swap entire points, not just one value
			x1 = x2;
			x2 = swap;
		}
		int slope = (x2-x1)*100/(y2-y1); //calculate change of x not exactly 'slope'
		for(int i = 0; i<abs(y2-y1); i++){
			int newx = x1+slope*i/100; //for every increase of i (by 1) calculate the x that should be filled
			ST7735_DrawPixel(newx,y1+i,color);
		}
	}
	else{
		if(x1>x2){ //start with lower value
			int swap = y1;
			y1 = y2;
			y2 = swap;
			swap = x1;
			x1 = x2;
			x2 = swap;
		}
		int slope = (y2-y1)*100/(x2-x1);
		for(int i = 0; i<abs(x2-x1); i++){
			int newy = y1+slope*i/100;
			ST7735_DrawPixel(x1+i,newy,color);
		}
	}
}




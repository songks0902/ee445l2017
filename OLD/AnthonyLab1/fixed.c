// ******** fixed.c ************** 
// possible header file for Lab 1 
// feel free to change the specific syntax of your system
// Anthony Bauer, Kaisheng Song
// Description: Using ARM Cortex M processor to develop fixed-point output routines
// Lab Number: TTu 2-330
// TA: COREY CORMIER
// Initial Creation Date: 08/30/2016
// Date of last revision: 09/06/2016

#include <stdint.h>
#include "fixed.h"
#include "ST7735.h"

int maxX, maxY, minX, minY, xSlope, xRange, ySlope, yRange;

/*
 * @brief 	switch the inout number to signed 32-bit decimal fixed-point ? = 0.001
 * @param int32_t n		input number
 * @ouput print out the number on the LCD screen
 */
void ST7735_sDecOut3(int32_t n){
		if(n>9999 || n<-9999){ 	//determine if outside of display range
			ST7735_OutChar(32); 	//space
			ST7735_OutChar(42); 	//star
			ST7735_OutChar(46); 	//period
			ST7735_OutChar(42);
			ST7735_OutChar(42);
			ST7735_OutChar(42);
			return;
		}
			
		if(n<0){ 	//take care of negative sign
			ST7735_OutChar(45);
			n*=-1; 	//we already took care of this, dont mess up the output
		}
		else{
			ST7735_OutChar(32);
		}
		
		int digit; 	//current digit to display
		int power = 1000; 	//power of digit
		while(power>0){ 	//get all digits
			digit = n/power; 	//collect current digit
			n %= power; 	//remove collected digits
			ST7735_OutChar(digit+48); 	//output current digit
			if(power == 1000){ 	//when the period needs to be placed
				ST7735_OutChar(46);
			}
			power/=10; 	//lower the power
		}
}

/*
 * @brief 	switch the inout number to unsigned 32-bit binary fixed-point ?= 1/256
 * @param int32_t n		input number
 * @ouput print out the number on the LCD screen
 */
void ST7735_uBinOut8(uint32_t n){
		if(n>=256000){ 	//too large to display
				ST7735_OutChar(42); 	//star
				ST7735_OutChar(42);
				ST7735_OutChar(42);
				ST7735_OutChar(46); 	//period
				ST7735_OutChar(42);
				ST7735_OutChar(42);
				return;
		}
		//with some tricky math we keep 5 digits (so we can round if needed)
		int large = n*125/32;  //converts binary to decimal
		int power = 100000;		//power of digit to display
		int digit = 0;		//digit to display
		int flag = 0;		//when we start displaying
		while(power>1){
			digit = large/power;		//determine current digit
			large %= power;		//remove front digit
			
			if(digit!=0||flag){		//if its not zero or if weve already started
				flag=1;		//weve started
				ST7735_OutChar(48+digit);		//display digit
			}
			else{
				if(power==1000)		//time for period
					ST7735_OutChar(48);  //zero infront of period
				else{
					ST7735_OutChar(32); //space since weve not started
				}
			}
			if(power==1000){  //time for period
				flag=2;  //weve stated displaying
				ST7735_OutChar(46);		//period
			}
			
			power/=10;		//reduce power
		}
}

/*
 * @brief 	Specify the X and Y axes for an x-y scatter plot
 * @param title 	string of graph title
 * @prarm minx		smallest X data value allowed, delta= 0.001
 * @prarm maxx		largest X data value allowed, delta= 0.001 
 * @prarm miny		smallest Y data value allowed, delta= 0.001
 * @prarm maxx		largest Y data value allowed, delta= 0.001
 * @ouput print out the x-y axes on the LCD screen
 * plot is 127 by 127  x plot ranges 0 -> 127 (in pixels)
 * y is 160 large			y plot ranges 159 -> 32 (in pixels)
 */
void ST7735_XYplotInit(char* title, int32_t minx, int32_t maxx, int32_t miny, int32_t maxy){
		ST7735_FillScreen(0);		//clear the screen for this plot
		
		maxX = maxx;		//save these for Ploting
		minX = minx;
		maxY = maxy;
		minY = miny;

		xRange = maxX-minX;		//range based on resolution
		xSlope = xRange/127; 		//number of resolution per pixel
		if(xRange-xSlope*127>64){	//round it up if more than 64 resolution off screen
			xSlope++;
		}
		
		yRange = maxY-minY;		//same comments as xRange & xSlope
		ySlope = yRange/127;
		if(yRange-ySlope*127>64){
			ySlope++;
		}
		
		ST7735_DrawString(1,1,title,0x07FF);		//draw the title, but not in the corner
		
		if(maxX<0){
			ST7735_DrawFastVLine(127,32,128,0x077FF);		//if the axis should be on right of screen
		}
		else if(minX<0){
			ST7735_DrawFastVLine((minX*-1)/xSlope, 32, 128, 0x07FF);		//if the axis could be in the middle
		}
		else{
			ST7735_DrawFastVLine(0,32,128,0x07FF);		//axis on left
		}
		
		if(maxY<0){
			ST7735_DrawFastHLine(0,32,128,0x07FF);		//if the axis should be on the top of screen
		}
		else if(minY<0){
			ST7735_DrawFastHLine(0,159-(minY*-1)/ySlope, 128, 0x07FF);		//if the axis could be in the middle 
		}
		else{
			ST7735_DrawFastHLine(0, 159, 128, 0x07FF);		//axis on bottom
		}
}

/*
 * @brief					plots the X-Y scatter data on x-y axes
 * @param num			number of points in the X, Y arrays 
 * @param bufX		array of X data, delta= 0.001
 * @param bufY		array of Y data, delta= 0.001 
 */
void ST7735_XYplot(uint32_t num, int32_t bufX[], int32_t bufY[]){
		for(int i = 0; i<num; i++){		//for every pair of x,y
				if(bufX[i]<minX || bufX[i]>maxX || bufY[i]<minY || bufY[i]>maxY){ //if either x or y resolution is outside plot 
					continue;		//ignore and move to next pair
				}
				int x = (bufX[i]-minX)/xSlope; 	//find the pixel to color
				int y = (bufY[i]-minY)/ySlope;
				ST7735_DrawPixel(x,159-y,0xFFFF);		//color said pixel
		}
}


#include "PLL.h"
#include "inc\tm4c123gh6pm.h"
#include <stdint.h>
#include "Timer.h"
#include "Switch.h"
#include "Stepper.h"
#include "State.h"

// Color    LED(s) PortF
// dark     ---    0
// red      R--    0x02
// blue     --B    0x04
// green    -G-    0x08
// yellow   RG-    0x0A
// sky blue -GB    0x0C
// white    RGB    0x0E
// pink     R-B    0x06

#define PF2             (*((volatile uint32_t *)0x40025010))
#define PF1             (*((volatile uint32_t *)0x40025008))

int main(void){
	PLL_Init();
	Switch_Init();
	Stepper_Init();
	Timer_Init();
	State_Init();
	
	while(1){ PF1 ^= 0x02; }
}

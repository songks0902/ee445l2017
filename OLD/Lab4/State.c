#include "inc\tm4c123gh6pm.h"
#include "Stepper.h"
#include "Switch.h"
#include "Timer.h"

const int immediate = 100;
const int slow = 10000;		//10ms

int currentState;
void (*ensureFunctionCalled)(void);

typedef struct{
	void (*action) (void);
	int nextState[8];
	int delay;
} MotorState;

MotorState states[] = {
	{Stepper_Dummy,		{0,19,2,19,1,19,3,19}, 	slow},
	{Stepper_CW,		{0,19,2,19,1,19,3,19}, 	slow},
	{Stepper_CCW,		{0,19,2,19,1,19,3,19}, 	slow},
	//3
	{Stepper_CW,		{4,4,4,4,4,4,4,4}, 		immediate},
	{Stepper_CW,		{5,5,5,5,5,5,5,5}, 		immediate},
	{Stepper_CW,		{6,6,6,6,6,6,6,6}, 		immediate},
	{Stepper_CW,		{7,7,7,7,7,7,7,7}, 		immediate},
	//7
	{Stepper_CW,		{8,8,8,8,8,8,8,8}, 		immediate},
	{Stepper_CW,		{9,9,9,9,9,9,9,9}, 		immediate},
	{Stepper_CW,		{10,10,10,10,10,10,10,10}, 	immediate},
	{Stepper_CW,		{11,11,11,11,11,11,11,11}, 	immediate},
	//11
	{Stepper_CCW,		{12,12,12,12,12,12,12,12}, 	immediate},
	{Stepper_CCW,		{13,13,13,13,13,13,13,13}, 	immediate},
	{Stepper_CCW,		{14,14,14,14,14,14,14,14}, 	immediate},
	{Stepper_CCW,		{15,15,15,15,15,15,15,15}, 	immediate},
	//15
	{Stepper_CCW,		{16,16,16,16,16,16,16,16}, 	immediate},
	{Stepper_CCW,		{17,17,17,17,17,17,17,17}, 	immediate},
	{Stepper_CCW,		{18,18,18,18,18,18,18,18}, 	immediate},
	{Stepper_CCW,		{0,19,2,19,1,19,3,19}, 	immediate},
	//19
	{Stepper_Dummy,		{20,19,20,19,20,19,20,19}, 	slow},
	{Stepper_Step,		{0,19,2,19,1,19,3,19}, 	slow},
};

void State_Init(void){
	currentState = 0;
}

void Timer0A_Handler(void){
	TIMER0_ICR_R = TIMER_ICR_TATOCINT;
	currentState = states[currentState].nextState[Switch_GetInputs()];
	Timer_SetTimeout(states[currentState].delay);
	ensureFunctionCalled = *(states[currentState].action);
}


#ifndef Stepper_H
#define Stepper_H

void Stepper_Init(void);

void Stepper_CW(void);
void Stepper_CCW(void);
void Stepper_Step(void);
void Stepper_Dummy(void);

#endif

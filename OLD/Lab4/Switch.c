#include "inc\tm4c123gh6pm.h"

#define INPUTS (*((volatile uint32_t *)0x40024078)) //PE1-4

void Switch_Init(void){
	volatile uint32_t delay;
	SYSCTL_RCGC2_R |= 0x31; //turn on F E & A
	delay = SYSCTL_RCGCGPIO_R;	//NOP

	//external switches
	GPIO_PORTE_DEN_R |= 0x1D; //bits 1-4
	GPIO_PORTE_DIR_R &= ~0x1D;
	GPIO_PORTE_AMSEL_R &= ~0x1D;
	GPIO_PORTE_AFSEL_R &= ~0x1D;
	GPIO_PORTE_PDR_R |= 0x1D; //positive logic -> P-D R's
	
	GPIO_PORTF_DEN_R |= 0x1E; //bits 1-4
	GPIO_PORTF_DIR_R |= 0x1E;
	GPIO_PORTF_AMSEL_R &= ~0x1E;
	GPIO_PORTF_AFSEL_R &= ~0x1E;
}

int Switch_GetInputs(void){
	return INPUTS>>1;
}

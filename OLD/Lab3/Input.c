
#include "inc\tm4c123gh6pm.h"
#include "Input.h"

#define GPIO_PORTE1				(*((volatile uint32_t *)0x40024008))
#define GPIO_PORTE2				(*((volatile uint32_t *)0x40024010))
#define GPIO_PORTE3				(*((volatile uint32_t *)0x40024020))
#define GPIO_PORTE4				(*((volatile uint32_t *)0x40024040))

void Input_Init(void){
	volatile uint32_t delay;
	SYSCTL_RCGC2_R |= 0x31; //turn on F E & A
	delay = SYSCTL_RCGCGPIO_R;	//NOP

	//external switches
	GPIO_PORTE_DEN_R |= 0x1D; //bits 1-4
	GPIO_PORTE_DIR_R &= ~0x1D;
	GPIO_PORTE_AMSEL_R &= ~0x1D;
	GPIO_PORTE_AFSEL_R &= ~0x1D;
	GPIO_PORTE_PDR_R |= 0x1D; //positive logic -> P-D R's
	
	GPIO_PORTF_DEN_R |= 0x1F; //bits 0-4
	GPIO_PORTF_DIR_R |= 0x1F;
	GPIO_PORTF_AMSEL_R &= ~0x1F;
	GPIO_PORTF_AFSEL_R &= ~0x1F;
}

void Timer0A_Handler(void){		//Buttons Timer
	TIMER0_ICR_R = TIMER_ICR_TATOCINT;    // acknowledge timer0A timeout
	inactive++;
	if(GPIO_PORTE1){	//set clock
		if(!flag[0])
			flag[0]=3;	//set high
		inactive=0;
	}
	else{
		if(flag[0]==3)
			flag[0]=1;
	}
	if(GPIO_PORTE2){	//set alarm		
		if(flag[1]==0)
			flag[1]=3;
		inactive=0;
	}
	else{
		if(flag[1]==3)
			flag[1]=1;
	}
	if(GPIO_PORTE3){	//change display
		if(flag[2]==0)
			flag[2]=3;
		inactive=0;
	}
	else{
		if(flag[2]==3)
			flag[2]=1;
	}
	if(GPIO_PORTE4){	//turn alarm on/off
		if(flag[3]==0)
			flag[3]=3;
		inactive=0;
	}
	else{
		if(flag[3]==3)
			flag[3]=1;
	}
	
	if(flag[0]==2)
		flag[0]=0;		//set low
	if(flag[1]==2)
		flag[1]=0;
	if(flag[2]==2)
		flag[2]=0;
	if(flag[3]==2)
		flag[3]=0;
}

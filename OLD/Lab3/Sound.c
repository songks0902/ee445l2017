#include "Sound.h"
#include "inc\tm4c123gh6pm.h"

#define A    1420   // 880 Hz
#define GPIO_PORTF4             (*((volatile uint32_t *)0x40025040))

int halfcycle;

void Sound_Init(void){
	
}

void Sound_Start(){
	TIMER3_CTL_R = 0x00000001;
}

void Sound_Stop(){
	TIMER3_CTL_R = 0x0;
}

void Timer3A_Handler(void){
	TIMER3_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER3A timeout
	halfcycle ^= 1;
	if(halfcycle){
		GPIO_PORTF4 = 0x10;
	}
	else{
		GPIO_PORTF4 = 0x0;
	}	
}

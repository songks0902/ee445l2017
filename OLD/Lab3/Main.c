
#include "PLL.h"
#include "inc\tm4c123gh6pm.h"
#include <stdint.h>
#include "Timer.h"
#include "Input.h"
#include "Screen.h"
#include "ST7735.h"
#include "Sound.h"

// Color    LED(s) PortF
// dark     ---    0
// red      R--    0x02
// blue     --B    0x04
// green    -G-    0x08
// yellow   RG-    0x0A
// sky blue -GB    0x0C
// white    RGB    0x0E
// pink     R-B    0x06

#define GPIO_PORTF2             (*((volatile uint32_t *)0x40025010))
#define GPIO_PORTF1             (*((volatile uint32_t *)0x40025008))


int flag[6]; //0-3 buttons 4 is screen/update 5 is sound
int inactive;

int minutes, hours;
int prevmin, prevhour;

int setmin, sethour;
int alarmmin, alarmhour;


int getSine(int deg){
	int sign = -1;
	if(deg<0 || deg > 180)
		sign = 1;
	deg+=180;
	deg %= 180;
	int num = 4*deg*(180-deg);
	num*=1000*sign;
	int den = 40500 - deg*(180-deg);
	return num/den;
}

void drawHands(uint16_t min, uint16_t hour, uint16_t mincolor, uint16_t hourcolor){
	int sin = getSine(min*6-90);
	int cos = getSine(min*6-180);
	
	ST7735_Line(64+cos*5/1000,96-sin*5/1000,64+cos*30/1000,96-sin*30/1000,mincolor);
	
	sin = getSine(hour*15-90);
	cos = getSine(hour*15-180);
	ST7735_Line(64+cos*5/1000,96-sin*5/1000,64+cos*15/1000,96-sin*15/1000,hourcolor);
	
	prevmin = min;
	prevhour = hour;
}

void drawDigital(int minutes, int hours){
	ST7735_FillScreen(BACKGROUNDCOLOR);
	char c = (hours/10)+48;
	ST7735_DrawChar(35, 80, c, 0x001F, BACKGROUNDCOLOR, 2);
	c = (hours%10)+48;
	ST7735_DrawChar(47, 80, c, 0x001F, BACKGROUNDCOLOR, 2);
	c = ':';
	ST7735_DrawChar(57, 80, c, 0x001F, BACKGROUNDCOLOR, 2);
	c = (minutes/10)+48;
	ST7735_DrawChar(67, 80, c, 0x07E0, BACKGROUNDCOLOR, 2);
	c = (minutes%10)+48;
	ST7735_DrawChar(80, 80, c, 0x07E0, BACKGROUNDCOLOR, 2);
}

void updateScreen(void);

void setAlarm(){
	setmin = alarmmin;
	sethour = alarmhour;
	flag[4]=1;
}

void setTime(){
	setmin = minutes;
	sethour = hours;
	flag[4]=1;
}

void toggleAlarm(){
	
	flag[4] = 1;
}

void toggleDisplay(){
	flag[4] = 1;
}

void setHourUp(){
	sethour++;
}

void setMinuteUp(){
	setmin++;
}

void dummy(){ }

void alarmConfirm(){
	alarmhour = sethour;
	alarmmin = setmin;
}

void timeConfirm() {
	hours = sethour;
	minutes = setmin;
}

typedef struct{
	int displayState;
	void (*actions[4]) (void);
	int nextState[4];
} WatchState;

WatchState states[] = { //displayState , actions, nextState
	{0x00,{setAlarm,setTime,toggleAlarm,toggleDisplay},{4,8,1,2}},		//normal watch face
	{0x10,{setAlarm,setTime,toggleAlarm,toggleDisplay},{5,9,0,3}},		//normal watch face, alarm
	{0x01,{setAlarm,setTime,toggleAlarm,toggleDisplay},{6,10,3,0}},		//digital watch face
	{0x11,{setAlarm,setTime,toggleAlarm,toggleDisplay},{7,11,2,1}},		//digital watch face, alarm
	//4
	{0x02,{setHourUp,alarmConfirm,dummy,setMinuteUp},{99,0,99,99}},	//setAlarm, normal watch face
	{0x12,{setHourUp,alarmConfirm,dummy,setMinuteUp},{99,1,99,99}},	//setAlarm, normal watch face, alarm
	{0x02,{setHourUp,alarmConfirm,dummy,setMinuteUp},{99,2,99,99}},	//setAlarm, digital watch face
	{0x12,{setHourUp,alarmConfirm,dummy,setMinuteUp},{99,3,99,99}},	//setAlarm, digital watch face, alarm
	//8
	{0x03,{setHourUp,timeConfirm,dummy,setMinuteUp},{99,0,99,99}},	//setClock, normal watch face
	{0x13,{setHourUp,timeConfirm,dummy,setMinuteUp},{99,1,99,99}},	//setClock, normal watch face, alarm
	{0x03,{setHourUp,timeConfirm,dummy,setMinuteUp},{99,2,99,99}},	//setClock, digital watch face
	{0x13,{setHourUp,timeConfirm,dummy,setMinuteUp},{99,3,99,99}}	//setClock, digital watch face, alarm
	//12
};

int currentState;
void (*ensureFunctionCalled)(void);

int main(void){
	PLL_Init();
	Input_Init();
	Timer_Init();
	Screen_Init();
	currentState = 0;
	
	while(1){
		for(int i = 0; i < 4; i++){
			if(flag[i]==1){
				flag[5]=2;
				ensureFunctionCalled = *(states[currentState].actions)[i];
				if(states[currentState].nextState[i]!=99)
					currentState = states[currentState].nextState[i];
				flag[i]=2;
			}
		}
		if(inactive>=50*5){
			flag[4]=1;
			inactive = 0;
			if(currentState>3)
				currentState = states[currentState].nextState[1];
		}
		updateScreen();
	}
}

void updateScreen(){
	if(flag[4] || minutes!=prevmin || hours!=prevhour){
		int displayState = states[currentState].displayState&0x0F;
		int alarmState = states[currentState].displayState&0x10;
		if(!displayState){ //normalwatchface
			if(flag[4])
				Screen_Background();
			drawHands(prevmin,prevhour,0xC618,0xC618);
			drawHands(minutes,hours,0x07E0,0x001F);
		}
		else if(displayState==1){
			drawDigital(minutes,hours);
		}
		else{
			drawDigital(setmin,sethour);
		}
		
		if(alarmState){
			char alarm[] = "ALARM"; 
			ST7735_DrawString(0,30,alarm,0x07E0);
		}
		
		if(displayState>=2){
			char set[] = "SET"; 
			ST7735_DrawString(50,30,set,0x07E0);
		}
		
		flag[4]=0;
	}
	
	if(hours==alarmhour&&minutes==alarmmin&&!flag[5]){
		Sound_Start();
		flag[5]=1;
	}
	else{
		flag[5]=0;
	}
	
	
}

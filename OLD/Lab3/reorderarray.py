import sys

if len(sys.argv) < 2:
	print('Usage: [Filename.***] [width] [height]')
	sys.exit(1)
if('.' in sys.argv[1]):
	name = sys.argv[1][:(sys.argv[1].index("."))]
else:
	name = sys.argv[1]
file = open((name+".txt"), "r+")
pixels = []
unused = []
for line in file:
	if(',' in line):
		for element in line[1:-2].split(', '):
			pixels.append(str(element))
	else:
		unused.append(line)
file.seek(0,0)
file.truncate();
file.write(unused[0])
for count in xrange(0, len(pixels), int(sys.argv[2])):
	for element in pixels[count:count+int(sys.argv[2])]:
		file.write((" "+str(element)+",").replace(",,",""))
	file.write("\n")
for line in unused[1:]:
	file.write(line)
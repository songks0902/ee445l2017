// ADCTestMain.c
// Runs on TM4C123
// This program periodically samples ADC channel 0 and stores the
// result to a global variable that can be accessed with the JTAG
// debugger and viewed with the variable watch feature.
// Daniel Valvano
// September 5, 2015

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015

 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

// center of X-ohm potentiometer connected to PE3/AIN0
// bottom of X-ohm potentiometer connected to ground
// top of X-ohm potentiometer connected to +3.3V 
#include <stdint.h>
#include "ADCSWTrigger.h"
#include "../inc/tm4c123gh6pm.h"
#include "PLL.h"
#include "ST7735.h"

#define PF2             (*((volatile uint32_t *)0x40025010))
#define PF1             (*((volatile uint32_t *)0x40025008))
	
#define True 1
#define False 0

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode
void ST7735_Line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, 
                 uint16_t color) ;

void Init_screen();

uint32_t TimeArray[100];
uint32_t AdcArray[100];

uint32_t recorded = 0;

uint32_t is_full = False;

volatile uint32_t ADCvalue;
// This debug function initializes Timer0A to request interrupts
// at a 100 Hz frequency.  It is similar to FreqMeasure.c.
void Timer0A_Init100HzInt(void){
  volatile uint32_t delay;
  DisableInterrupts();
  // **** general initialization ****
  SYSCTL_RCGCTIMER_R |= 0x01;      // activate timer0
  delay = SYSCTL_RCGCTIMER_R;      // allow time to finish activating
  TIMER0_CTL_R &= ~TIMER_CTL_TAEN; // disable timer0A during setup
  TIMER0_CFG_R = 0;                // configure for 32-bit timer mode
  // **** timer0A initialization ****
                                   // configure for periodic mode
  TIMER0_TAMR_R = TIMER_TAMR_TAMR_PERIOD;
  //TIMER0_TAILR_R = 799999;         // start value for 100 Hz interrupts
	TIMER0_TAILR_R = 79999;//change to 1000hz
  TIMER0_IMR_R |= TIMER_IMR_TATOIM;// enable timeout (rollover) interrupt
  TIMER0_ICR_R = TIMER_ICR_TATOCINT;// clear timer0A timeout flag
  TIMER0_CTL_R |= TIMER_CTL_TAEN;  // enable timer0A 32-b, periodic, interrupts
  // **** interrupt initialization ****
                                   // Timer0A=priority 2
  NVIC_PRI4_R = (NVIC_PRI4_R&0x00FFFFFF)|0x40000000; // top 3 bits
  NVIC_EN0_R = 1<<19;              // enable interrupt 19 in NVIC
}
void Timer0A_Handler(void){
  TIMER0_ICR_R = TIMER_ICR_TATOCINT;    // acknowledge timer0A timeout
  PF2 ^= 0x04;                   // profile
  PF2 ^= 0x04;                   // profile
  ADCvalue = ADC0_InSeq3();
	//is_full = True;
	if(recorded < 100) {
		//TimeArray[recorded] =TIMER1_TAR_R;
		AdcArray[recorded] = ADCvalue;
		recorded += 1;
	}else {
		is_full = True;
	}
	
  PF2 ^= 0x04;                   // profile
}


void Timer1A_Init() {
	volatile uint32_t delay;
	SYSCTL_RCGCTIMER_R |= 0X02;
	delay = SYSCTL_RCGCTIMER_R;
	
	TIMER1_CTL_R = 0x00000000;
	TIMER1_CFG_R = 0x00000000;
	TIMER1_TAMR_R = 0x00000002;
	
	
	TIMER1_TAILR_R = 0xFFFFFFFF - 1; // 80000000/(1/53)
	TIMER1_TAPR_R = 0;
	
	TIMER1_CTL_R = 0x00000001;
	
	
	
	
	
}


void Timer2_Init(){
  SYSCTL_RCGCTIMER_R |= 0x04;   // 0) activate timer2
  TIMER2_CTL_R = 0x00000000;    // 1) disable timer2A during setup
  TIMER2_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER2_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER2_TAILR_R = 800;    // 4) reload value
  TIMER2_TAPR_R = 0;            // 5) bus clock resolution
  TIMER2_ICR_R = 0x00000001;    // 6) clear timer2A timeout flag
  TIMER2_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0x00FFFFFF)|0x20000000; // 8) priority 1
// interrupts enabled in the main program after all devices initialized
// vector number 39, interrupt number 23
  NVIC_EN0_R = 1<<23;           // 9) enable IRQ 23 in NVIC
  TIMER2_CTL_R = 0x00000001;    // 10) enable timer2A
}

void Timer2A_Handler(void){
  TIMER2_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER2A timeout
}

void Timer3_Init(){
  SYSCTL_RCGCTIMER_R |= 0x08;   // 0) activate TIMER3
  TIMER3_CTL_R = 0x00000000;    // 1) disable TIMER3A during setup
  TIMER3_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER3_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER3_TAILR_R = 800;    // 4) reload value
  TIMER3_TAPR_R = 0;            // 5) bus clock resolution
  TIMER3_ICR_R = 0x00000001;    // 6) clear TIMER3A timeout flag
  TIMER3_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI8_R = (NVIC_PRI8_R&0x00FFFFFF)|0x20000000; // 8) priority 1
// interrupts enabled in the main program after all devices initialized
// vector number 51, interrupt number 35
  NVIC_EN1_R = 1<<(35-32);      // 9) enable IRQ 35 in NVIC
  TIMER3_CTL_R = 0x00000001;    // 10) enable TIMER3A
}

void Timer3A_Handler(void){
  TIMER3_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER3A timeout
}


/*record the time difference*/
uint32_t difference[999];

uint32_t pdf[4096];

int main(void){
  PLL_Init(Bus80MHz);                   // 80 MHz
  SYSCTL_RCGCGPIO_R |= 0x20;            // activate port F
  ADC0_InitSWTriggerSeq3_Ch9();         // allow time to finish activating
  Timer0A_Init100HzInt();               // set up Timer0A for 100 Hz interrupts
  
	/*for prep 3  initializes one of the 
	32-bit timers to count every 12.5ns 
	continuously*/
	//Timer2_Init();
	//Timer3_Init();
	//Timer1A_Init();
	
	GPIO_PORTF_DIR_R |= 0x06;             // make PF2, PF1 out (built-in LED)
  GPIO_PORTF_AFSEL_R &= ~0x06;          // disable alt funct on PF2, PF1
  GPIO_PORTF_DEN_R |= 0x06;             // enable digital I/O on PF2, PF1
                                        // configure PF2 as GPIO
  GPIO_PORTF_PCTL_R = (GPIO_PORTF_PCTL_R&0xFFFFF00F)+0x00000000;
  GPIO_PORTF_AMSEL_R = 0;               // disable analog functionality on PF
  PF2 = 0;                      // turn off LED
  EnableInterrupts();
  while(is_full == 0){ //! is_full
    PF1 ^= 0x02;  // toggles when running in main
		//GPIO_PORTF_DATA_R ^= 0x02;
		//PF1 = (PF1*12345678)/1234567+0x02;
	}
	
	uint32_t max_diff = 0;
	uint32_t min_diff = (uint32_t)0xFFFFFFFFF;
	uint32_t minimum = (uint32_t)0xFFFFFFFFF;
	uint32_t maximum = 0;
	
	
	for(int i = 0; i < 999; i+=1) {
		difference[i] = TimeArray[i] - TimeArray[i+1];
		
		if(difference[i] < min_diff) {
			min_diff = difference[i];
		} else if(difference[i] > max_diff) {
			max_diff = difference[i];
			
		}
		
		/*if(AdcArray[i+1] < minimum) {
			minimum = AdcArray[i+1];
		} else if(AdcArray[i+1] > maximum) {
			maximum = AdcArray[i+1];
		}*/
	}
		
	uint32_t time_jitter = max_diff - min_diff;
		
	
	/*need to finish prep 6*/
	for(int i = 0; i < 1000; i+=1) {
		pdf[AdcArray[i]] += 1;

	}
		
	int temp = 0;
	
	
	/*try to output the file to screen*/
	Init_screen();
	int x1 = 0;
	int x2 = 0;
	int y1 = 0;
	int y2 = 0;
	for(int i = 0; i < 4095; i+=1) {
		x1 = 127 - ((i*127)/4095);
		x2 = x1;
		y1 = (pdf[i] *159)/1000;
		y2 = 0;
		
		ST7735_Line(x1, y1, x2, y2, 0xFFFF);
		
	}
		
}

void Init_screen()
{
	ST7735_InitR(INITR_REDTAB);
	ST7735_FillScreen(0x07E0);  // set screen to black
	ST7735_SetCursor(0,0);
	
}

int main2(void){
	PLL_Init(Bus80MHz);                          
	ST7735_InitR(INITR_REDTAB);
	ST7735_FillScreen(0x07E0);  // set screen to black
	ST7735_SetCursor(0,0);
	
	ST7735_Line(50,80,50,0,0xFFFF); // 12
	ST7735_Line(50,80,80,20,0xFFFF);// 1
	ST7735_Line(50,80,100,80,0xFFFF);// 3
	ST7735_Line(50,80,80,120,0xFFFF);// 5 
	ST7735_Line(50,80,50,150,0xFFFF); // 6
	ST7735_Line(50,80,30,120,0xFFFF); // 7
	ST7735_Line(50,80,0,80,0xFFFF); // 9
	ST7735_Line(50,80,30,40,0xFFFF); // 11
	return  1;
}


int abs(int num) {
	
	if(num < 0) {
		return num *= -1;
	}
	
	return num;
	
}



//************* ST7735_Line********************************************
//  Draws one line on the ST7735 color LCD
//  Inputs: (x1,y1) is the start point
//          (x2,y2) is the end point
// x1,x2 are horizontal positions, columns from the left edge
//               must be less than 128
//               0 is on the left, 126 is near the right
// y1,y2 are vertical positions, rows from the top edge
//               must be less than 160
//               159 is near the wires, 0 is the side opposite the wires
//        color 16-bit color, which can be produced by ST7735_Color565() 
// Output: none
void ST7735_Line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, 
                 uint16_t color) 
{
		if(abs(x2-x1)<=abs(y2-y1)){ //find the larger value, that way we can iterate over that one
		if(y1>y2){ //since our loop increases by 1 pixel each time we need to start with the lower value
			int swap = y1;
			y1 = y2;
			y2 = swap;
			swap = x1; //swap entire points, not just one value
			x1 = x2;
			x2 = swap;
		}
		int slope = (x2-x1)*100/(y2-y1); //calculate change of x not exactly 'slope'
		for(int i = 0; i<abs(y2-y1); i++){
			int newx = x1+slope*i/100; //for every increase of i (by 1) calculate the x that should be filled
			ST7735_DrawPixel(newx,y1+i,color);
		}
	}
	else{
		if(x1>x2){ //start with lower value
			int swap = y1;
			y1 = y2;
			y2 = swap;
			swap = x1;
			x1 = x2;
			x2 = swap;
		}
		int slope = (y2-y1)*100/(x2-x1);
		for(int i = 0; i<abs(x2-x1); i++){
			int newy = y1+slope*i/100;
			ST7735_DrawPixel(x1+i,newy,color);
		}
	}


}
	
	



